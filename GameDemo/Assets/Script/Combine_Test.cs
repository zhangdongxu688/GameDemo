﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Combine_Test : MonoBehaviour 
{
    void Start()
    {
        MeshRenderer[] childs = gameObject.transform.GetComponentsInChildren<MeshRenderer>();
        Material[] materials = new Material[childs.Length];
        for (int i = 0; i < childs.Length;i++)
        {
            materials[i] = childs[i].sharedMaterial;
        }

        MeshFilter[] meshFilters = gameObject.transform.GetComponentsInChildren<MeshFilter>();

        CombineInstance[] combineInstances = new CombineInstance[meshFilters.Length];
        for (int i = 0; i < meshFilters.Length;i++)
        {
            combineInstances[i].mesh = meshFilters[i].sharedMesh;
            combineInstances[i].transform = meshFilters[i].transform.localToWorldMatrix;
            meshFilters[i].gameObject.SetActive(false);
        }

        transform.GetComponent<MeshFilter>().mesh = new Mesh();
        transform.GetComponent<MeshFilter>().mesh.CombineMeshes(combineInstances,false);
        transform.gameObject.SetActive(true);

        transform.GetComponent<MeshRenderer>().sharedMaterials = materials;
    }
	
}
