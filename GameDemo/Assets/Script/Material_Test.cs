﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Material_Test : MonoBehaviour 
{
	// Use this for initialization
	void Start () 
    {
        MeshRenderer meshRenderer = gameObject.GetComponent<MeshRenderer>();
        //meshRenderer.sharedMaterial = new Material (Shader.Find ("Standard"));
        //meshRenderer.sharedMaterial.color = Color.black;
        //Debug.LogError(meshRenderer.material == meshRenderer.sharedMaterial);

        meshRenderer.material.color = Color.black;
        meshRenderer.sharedMaterial = new Material(Shader.Find("Standard"));
        //meshRenderer.sharedMaterial.color = Color.white;

        //meshRenderer.material = new Material(Shader.Find("Standard"));
        //meshRenderer.sharedMaterial.color = Color.white;
        Debug.LogError(meshRenderer.material == meshRenderer.sharedMaterial);
	}
	
}
