﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelCombine : MonoBehaviour 
{
    GameObject m_CombinedObj;
    List<SkinnedMeshRenderer> m_PartMeshRander = new List<SkinnedMeshRenderer>();
	public void Combine()
    {
        //如果存在已经合并的物体 则删除
        if(m_CombinedObj != null)
        {
            Destroy(m_CombinedObj);
        }
    }
}
