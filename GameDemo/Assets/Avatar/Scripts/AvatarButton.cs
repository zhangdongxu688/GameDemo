﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class AvatarButton : MonoBehaviour {

  public void OnValueChange(bool isOn)
    {
        if (isOn)
        {
            if (gameObject .name .Equals("boy"))
            {
                //AvatarSys.instance.SexChange();
                CreatModelInstance.Instance.CreatBasicModel(2);
                return;
            }  

            if(gameObject.name.Equals("girl"))
            {
                CreatModelInstance.Instance.CreatBasicModel(1);
                return;
            }

            string[] names = gameObject.name.Split('-');
            //AvatarSys.instance.OnChangePeople(names[0],names[1]);
            CreatModelInstance.Instance.SetPart(names[0],gameObject.name);
            switch (names[0])
            {
                case "pants":PlayAnimation("item_pants"); break;
                case "shoes":PlayAnimation("item_boots"); break;
                case "top":PlayAnimation("item_shirt"); break;
                default:
                    break;
            }
        }
    }
    public void PlayAnimation(string aniName)
    {
        Animation ani = GameObject.FindWithTag("Player").GetComponent<Animation>();
        if (!ani.IsPlaying(aniName))
        {
            ani.Play(aniName);
            ani.PlayQueued("idle1");
        }
    }
    public void LoadScenes()
    {
        SceneManager.LoadScene(1);
    }
}
