﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreatModelInstance : MonoBehaviour 
{
    private GameObject m_Model;
    private List<Transform> m_ModelHips = new List<Transform>();
    private Dictionary<string,GameObject> m_ModelParts = new Dictionary<string, GameObject>();
    private int m_Type = -1;

    public static CreatModelInstance _instance;
    public static CreatModelInstance Instance
    {
        get
        {
            return _instance;
        }
    }

	private void Awake()
	{
        _instance = this;
	}

    public void CreatBasicModel(int type)
    {
        if (m_Model != null)
            Destroy(m_Model);
        m_ModelHips.Clear();
        string modelName = "";
        m_Type = type;
        //女孩
        if(type == 1)
        {
            modelName = "FemaleTarget";

        }
        //男孩
        else
        {
            modelName = "MaleTarget";
        }

        GameObject obj = (GameObject)Resources.Load(modelName);
        m_Model = Instantiate(obj);
        Transform[] hips = m_Model.GetComponentsInChildren<Transform>();
        for (int i = 0; i < hips.Length; i++)
        {
            m_ModelHips.Add(hips[i]);
        }

        SetPart("face","face-1");
    }

    public void SetPart(string partName,string PartSources)
    {
        string sourcesPath = "";

        if(m_Type == 1)
        {
            sourcesPath = "FemaleParts/" + partName + "/" + PartSources;
        }

        GameObject obj = (GameObject)Resources.Load(sourcesPath);
        obj = Instantiate(obj);
        obj.transform.parent = m_Model.transform;

        if(m_ModelParts.ContainsKey(partName))
        {
            Destroy(m_ModelParts[partName]);
        }

        m_ModelParts[partName] = obj;
        SkinnedMeshRenderer meshRander = obj.GetComponent<SkinnedMeshRenderer>();

        //重刷一遍骨骼引用
        List<Transform> newBones = new List<Transform>();
        Transform[] bones = meshRander.bones;
        for (int i = 0; i < bones.Length;i++)
        {
            foreach(var item in m_ModelHips)
            {
                if(item.name == bones[i].name)
                {
                    newBones.Add(item);
                    //bones[i] = item;
                }
            }
        }

        meshRander.bones = newBones.ToArray();

        //把部件自带的骨骼删除
        Destroy(obj.transform.Find("Female_Hips").gameObject);

    }


	
}
